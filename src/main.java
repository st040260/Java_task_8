import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class main {
    public static void main(String[] args) {
        long a=25214903917L;
        long c=11L;
        long m = (long)Math.pow(2,48);

        Stream<Long> infiniteStream = Stream.iterate(16L, i -> (a*(long)i+c)%m);

        List<Long> result = infiniteStream
                .limit(300)
                .collect(Collectors.toList());
        System.out.println(result);
    }
}
